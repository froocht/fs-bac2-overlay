import pyroute2


class NeighborFilter:
    def __init__(self, ifindex):
        self.store = NeighborCache()
        # get list of known neighbor cache entries
        with pyroute2.IPRoute() as ipr:
            neigh_list = ipr.get_neighbours(ifindex=ifindex)
            for i in neigh_list:
                lladdr = ''
                ipaddr = ''
                for a in  i['attrs']:
                    if a[0] == 'NDA_LLADDR':
                        lladdr = a[1].replace(':', '')
                    elif a[0] == 'NDA_DST':
                        ipaddr = a[1].split('/')[0]
                self.store.set(lladdr, ipaddr)

    def different(self, mac, ip):
        return self.store.different(mac, ip)

    def update(self, mac, ip):
        try:
            # add
            self.store.add(mac, ip)
        except:
            # update
            self.store.set(mac, ip)



class NeighborCache:
    def __init__(self):
        self.store = {}


    def add(self, key, value):
        if key in self.store.keys():
            raise BaseException("Eintrag vorhanden.")
        else:
            self.store[key] = value

    def set(self, key, value):
        self.store[key] = value

    def get_by_key(self, key):
        try:
            return (key, self.store[key])
        except:
            raise

    def get_by_value(self, value):
        return [(k,v) for (k,v) in self.store.items() if value in v][0]


    def different(self, key, value):
        """
        Checks mac,ip tuple against cache.
        Returns True if a difference is found or if no such entry exists.
        Else returns False.
        """
        try:
            entry = self.get_by_key(key)
        except:
            return True
        else:
            if value in entry[1]:
                return False
            else:
                return True