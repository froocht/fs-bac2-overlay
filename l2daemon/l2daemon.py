import logging
from util import CONSTS
import NeighborTableHandler
import KVStoreHandler
import queue
import time
import signal
import sys


def exit_requested(sig, frame):
    for n in range(len(instances)):
        instances[n].stop.set()
        instances[n].join(5)
        sys.exit(0)


if __name__ == '__main__':
    logger = logging.getLogger('mainLogger')
    hdl = logging.StreamHandler()
    hdl.setLevel(CONSTS.LOGLEVEL)
    logger.addHandler(hdl)
    logger.setLevel(CONSTS.LOGLEVEL)

    fhdl = logging.FileHandler(CONSTS.LOGFILE)
    hdl.setLevel(CONSTS.LOGLEVEL)
    logger.addHandler(fhdl)


    announceq = queue.Queue()
    recvq = queue.Queue()

    threads = {
        NeighborTableHandler.NeighborTableHandler: ['42', recvq, announceq],
        KVStoreHandler.KVStoreHandler: [CONSTS.PREFIXMANAGER_IP, recvq, announceq]
               }

    instances = []
    n = len(threads)

    for i in range(n):
        t_type = threads.keys()[i]
        t_args = threads.values()[i]
        logger.info('Spawning thread {} with args {}'.format(t_type, t_args))
        instances.insert(i, t_type(*t_args))
        instances[i].start()

    print('Everything is running, press Ctrl-C to exit..')

    signal.signal(signal.SIGINT, exit_requested)
    signal.pause()



