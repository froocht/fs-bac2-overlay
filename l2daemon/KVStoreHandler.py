from openr.utils.consts import Consts
from openr.cli.utils import utils
from openr.clients import kvstore_subscriber
from openr.Lsdb import ttypes as lsdb_types

import CustomPrefixManagerClient

from openr.utils import socket, consts, ipnetwork, serializer

import logging
import threading
import zmq
import queue

from util import PFXMGRCMD

import traceback


class KVStoreHandler(threading.Thread):
    def __init__(self, host, recvq, announceq, kvstore_port=60001, prefixmgr_cmd_port=60011):
        super(KVStoreHandler, self).__init__()
        self.logger = logging.getLogger('mainLogger')

        self.announceq = announceq
        self.recvq = recvq

        self.stop = threading.Event()

        kvstore_url = "tcp://[{}]:{}".format(host, kvstore_port)
        self.kvstore_client = kvstore_subscriber.KvStoreSubscriber(zmq.Context(), kvstore_url)


        prefix_mgr_cmd_url = "tcp://[{}]:{}".format(host, prefixmgr_cmd_port)
        self.prefix_mgr_client = CustomPrefixManagerClient.CustomPrefixMgrClient(zmq.Context(), prefix_mgr_cmd_url)


        # get own host name from LM
        self.host_id = utils.get_connected_node_name(host, 60006)




    def get_prefixes_from_kvstore(self):
        try:
            msg = self.kvstore_client.listen()
            for key, value in msg.keyVals.items():
                if key.startswith(Consts.PREFIX_DB_MARKER):
                    _, reported_node_name = key.split(':', 1)
                    if reported_node_name == self.host_id:
                        self.logger.info('Received my own announcement, ignore.')
                        continue
                    try:
                        prefix_db = serializer.deserialize_thrift_object(value.value, lsdb_types.PrefixDatabase)
                        type5_entries = [p for p in prefix_db.prefixEntries if p.type is lsdb_types.PrefixType.TYPE_5]
                    except:
                        self.logger.error(traceback.format_exc())
                    else:
                        for entry in type5_entries:
                            # only filter relevant prefixes
                            self.logger.info('Received update from {}'.format(reported_node_name))
                            mac, vni = entry.data.split(',')
                            self.recvq.put( (ipnetwork.sprint_prefix(entry.prefix), mac, vni) )



        except zmq.error.Again as e:
            # this will fire most of the time
            pass


    def run(self):
        self.logger.info("KVStoreSnooper entering mainloop")
        while not self.stop.is_set():
            try:
                msg = self.announceq.get_nowait()
                self.logger.info("Announcing prefix " + msg[0])
            except queue.Empty:
                # no msg to dequeue
                pass
            else:
                try:
                    self.prefix_mgr_client.send_cmd_to_prefix_mgr(PFXMGRCMD.ANNOUNCE, msg[0], '{},{}'.format(msg[1], msg[2]), 'TYPE_5')
                except:
                    self.logger.info('Error announcing prefix ' + msg[0])
                    self.logger.info(traceback.format_exc())

            self.get_prefixes_from_kvstore()
        self.logger.info("KVStoreSnooper exiting mainloop")

