import logging

# noinspection PyClassHasNoInit
class STATE:
    INIT = 1
    RUN = 2
    EXIT = 3


# noinspection PyClassHasNoInit
class PFXMGRCMD:
    ANNOUNCE = 1


# noinspection PyClassHasNoInit
class CONSTS:
    PREFIXMANAGER_IP = 'localhost'
    PREFIX_MGR_CMD_PORT = 60011
    VXLAN_VNI = 42
    LOGLEVEL = logging.INFO
    LOGFILE = '/root/l2daemon.log'

