from openr.clients import prefix_mgr_client
from openr.PrefixManager import ttypes as prefix_mgr_types
from openr.Lsdb import ttypes as lsdb_types
from openr.utils import socket, consts, ipnetwork


class CustomPrefixMgrClient(prefix_mgr_client.PrefixMgrClient):
    """
    Slight modification of Open/R's PrefixMgrClient to allow for populating the data field in the prefix announcements.
    Most of this code is taken from Open/R sources.
    """
    def send_cmd_to_prefix_mgr(self, cmd, prefix, data, prefix_type='BREEZE'):
        TYPE_TO_VALUES = lsdb_types.PrefixType._NAMES_TO_VALUES
        if prefix_type not in TYPE_TO_VALUES:
            raise Exception('Unknown type {}. Use any of {}'.format(
                            prefix_type, ', '.join(TYPE_TO_VALUES.keys())))

        req_msg = prefix_mgr_types.PrefixManagerRequest()
        req_msg.cmd = cmd
        req_msg.type = TYPE_TO_VALUES[prefix_type]
        req_msg.prefixes = []
        req_msg.prefixes.append(lsdb_types.PrefixEntry(
                prefix=ipnetwork.ip_str_to_prefix(prefix),
                type=TYPE_TO_VALUES[prefix_type],
                data=data))

        self._prefix_mgr_cmd_socket.send_thrift_obj(req_msg)
        ret = self._prefix_mgr_cmd_socket.recv_thrift_obj(prefix_mgr_types.PrefixManagerResponse)
        return ret


