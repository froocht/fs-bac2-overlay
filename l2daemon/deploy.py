import subprocess
import sys

def deploy(hosts):
    cmds = [ """ssh root@{host} -C 'for p in `ps auxww | grep l2daemon.py | awk \{{print\ \$2\}}`; do kill -9 $p; done'""",


        'ssh root@{host} -C "pip install pyroute2"',
             'echo {host}', 'ssh root@{host} -C "rm -rf /root/l2daemon; mkdir /root/l2daemon"',  'scp -6 ./*.py root@\[{host}\]:/root/l2daemon', 'ssh root@{host} -C "chmod a+x /root/l2daemon/l2daemon.py"', 'ssh root@{host} -C "ip neigh flush nud permanent"', 'ssh root@{host} -C "screen -d -m python /root/l2daemon/l2daemon.py"' ]

    for h in hosts:
        for c in cmds:
            f = subprocess.Popen( [c.format(host=h)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            for i in f.communicate():
                if i is not '':
                    print(i)
        print('deployed to {}'.format(h))
if __name__ == '__main__':
    deploy(sys.argv[1:])