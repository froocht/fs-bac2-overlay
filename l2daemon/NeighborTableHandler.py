from pyroute2 import IPRoute

import threading
import zmq
import pprint
import logging
import traceback
import queue
import time
from pyroute2.netlink.rtnl import ndmsg
import NeighborCache

class MyThread(threading.Thread):
    def __init__(self, devidx, vni, queue, nbr_cache):
        super(MyThread, self).__init__()

        self.queue = queue
        self.ipr = IPRoute()
        self.logger = logging.getLogger('mainLogger')
        self.stop = threading.Event()
        self.stop.clear()
        self.devidx = devidx
        self.vni = vni
        self.nbr_cache = nbr_cache

    def run_base(self):
        self.logger.info('{} entering mainloop'.format(type(self)))

class WriteThread(MyThread):
    def __init__(self, devidx, vni, recvq, nbr_cache):
        super(type(self), self).__init__(devidx, vni, recvq, nbr_cache)

    def run(self):
        self.run_base()
        while not self.stop.is_set():
            try:
                msg =  self.queue.get_nowait()
                self.logger.info('WriteThread received update {}'.format(pprint.pformat(msg)))
            except queue.Empty:
                time.sleep(0.1)
                continue

            # TODO create 1st class object
            ip = msg[0].split('/')[0]
            mac = msg[1]
            mac_w_colons = ':'.join( [msg[1][a:a+2] for a in range(0,12,2) ] )

            if self.nbr_cache.different(mac, ip):
                self.ipr.neigh('set', dst=ip, lladdr=mac_w_colons, ifindex=self.devidx, state=ndmsg.states['permanent'])
                #self.ipr.fdb('add', dst=ip, lladdr=mac, ifindex=devidx)
                self.nbr_cache.update(mac, ip)
                self.logger.info('{0:<30}: {0:<18} {1:<18}'.format("Added/Updated Neighbor", mac, ip))
            else:
                self.logger.info('{0:<30}: {0:<18} {1:<18}'.format("Known Neighbor", mac, ip))
            time.sleep(0.1)

class WatchThread(MyThread):
    def __init__(self, devidx, vni,announceq, nbr_cache):
        super(type(self), self).__init__(devidx, vni, announceq, nbr_cache)

    def run(self):
        self.run_base()
        self.ipr.bind()

        while not self.stop.is_set():
            for message in self.ipr.get():
                self.logger.debug(pprint.pformat(message))
                # we only want to receive events for our interface of choice
                if message['event'] == 'RTM_NEWNEIGH' and message.get('ifindex') == self.devidx:
                    self.logger.info('WatchThread received netlink event {}'.format(message['event']))
                    try:
                        lladdr = message.get_attr('NDA_LLADDR').replace(':', '')
                        ipaddr = message.get_attr('NDA_DST')
                    except:
                        logging.error('malformed message')
                        continue

                    if ipaddr.lower().startswith('fe80'):
                        logging.error('ignore link local addresses')
                        continue

                    if not self.nbr_cache.different(lladdr, ipaddr):
                        logging.info('Not announcing known entry {0:18} {1:18}'.format(lladdr, ipaddr))
                        continue

                    if not '.' in ipaddr:
                        ipaddr += '/128'
                    else:
                        ipaddr += '/32'


                    try:
                        self.queue.put( (ipaddr, lladdr, self.vni) )
                        self.nbr_cache.update(lladdr, ipaddr)
                    except BaseException as e:
                        logging.error(traceback.format_exc())
                        logging.error('message: ' + pprint.pformat(message))
                time.sleep(0.1)
        self.logger.debug('WatchThread exiting')



class NeighborTableHandler(threading.Thread):

    def __init__(self, vni, recvq, accounceq):
        super(NeighborTableHandler, self).__init__()
        self.logger = logging.getLogger('mainLogger')

        self.vni = int(vni)
        self.recvq = recvq
        self.announceq = accounceq

        self.stop = threading.Event()

        with IPRoute() as ipr:
            try:
                self.devidx = ipr.link_lookup(ifname='br' + str(self.vni))[0]
            except:
                raise BaseException("could not find interface vxlan"+str(self.vni))
        self.nbr_cache = NeighborCache.NeighborFilter(self.devidx)




    def run(self):
        self.logger.info('NeighborTableHandler entering mainloop')
        watchthread = WatchThread(self.devidx, self.vni, self.announceq, self.nbr_cache)
        watchthread.start()
        writethread = WriteThread(self.devidx, self.vni, self.recvq, self.nbr_cache)
        writethread.start()

        self.stop.wait()

        self.logger.info('NeighborTableHandler exiting')
        writethread.stop.set()
        watchthread.stop.set()

        writethread.join()
        watchthread.join()

