#!/bin/sh
CWD=`pwd`

for i in `find . -name py`; do
    cd "$i"
    python setup.py sdist
    mv dist/* "$CWD"
    cd "$CWD"
done